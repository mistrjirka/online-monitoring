let hidden = true;
$(".om-filter-button").click(() => {
  if (hidden) {
    $(".om-filter").slideDown();
    hidden = false;
  } else {
    $(".om-filter").slideUp();
    hidden = true;
  }
});
