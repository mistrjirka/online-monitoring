$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    loop: false,
    margin: 29,
    nav: true,
    dots: true,
    navText: [
      "<div class='om-previous om-previous-cars'><img src='img/sipka-carousel.svg'></div>",
      "<div class='om-next om-next-cars'><img src='img/sipka-carousel.svg'></div>",
    ],
    responsive: {
      0: {
        items: 1,
      },
      550: {
        items: 2,
      },
      1100: {
        items: 3,
      },
      1500: {
        items: 4,
      },
    },
  });
  $('.owl-carousel').on('change.owl.carousel', function (e) {
    var visibleSlides = e.page.size;
    var prevBtn = $('.om-previous-cars');
    var nextBtn = $('.om-next-cars');
    prevBtn.prop('disabled', false);
    nextBtn.prop('disabled', false);
    if (e.namespace && e.property.name === 'position' && e.relatedTarget.relative(e.property.value) === 0) {
        prevBtn.prop('disabled', true);

    }
    if (e.namespace && e.property.name === 'position' && e.relatedTarget.relative(e.property.value) === e.relatedTarget.items().length - visibleSlides) {
        nextBtn.prop('disabled', true);
    }
  });
});
