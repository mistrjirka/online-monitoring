var open = false;
var intervalset = false;
var interval = 0;

var open2 = false;
var intervalset2 = false;
var interval2 = 0;

function isTouchDevice() {
  return (
    "ontouchstart" in window ||
    navigator.maxTouchPoints > 0 ||
    navigator.msMaxTouchPoints > 0
  );
}
if (isTouchDevice()) {
  $(".om-drop-link-monitorujeme").click(function () {
    $(".megamenu-monitorujeme").fadeIn(200);
  });
  $(window).click(function (e) {
    if (!e.target.className.includes("om-drop-link-sluzby")) {
      $(".megamenu-monitorujeme").fadeOut(200);
    }
  });
  $(".om-drop-link-sluzby").click(function () {
    $(".megamenu-sluzby").fadeIn(200);
  });
  $(window).click(function (e) {
    if (!e.target.className.includes("om-drop-link-sluzby")) {
      $(".megamenu-sluzby").fadeOut(200);
    }
  });
} else {
  $(window).mouseover(function () {
    if ($(".om-drop-link-sluzby:hover").length != 0) {
      open = true;
      if (intervalset) {
        clearInterval(interval);
        intervalset = false;
      }
      setTimeout(() => {
        open = false;
        intervalset = false;
      }, 1000);
    }
    if (
      $(".megamenu-sluzby:hover").length != 0 ||
      open ||
      $(".om-drop-link-sluzby:hover").length != 0
    ) {
      $(".megamenu-sluzby").fadeIn(200);
    } else {
      $(".megamenu-sluzby").fadeOut(200);
    }
  });

  $(window).mouseover(function () {
    if ($(".om-drop-link-monitorujeme:hover").length != 0) {
      open2 = true;
      if (intervalset2) {
        clearInterval(interval2);
        intervalset2 = false;
      }
      setTimeout(() => {
        open2 = false;
        intervalset2 = false;
      }, 1000);
    }
    if (
      $(".megamenu-monitorujeme:hover").length != 0 ||
      open2 ||
      $(".om-drop-link-monitorujeme:hover").length != 0
    ) {
      $(".megamenu-monitorujeme").fadeIn(200);
    } else {
      $(".megamenu-monitorujeme").fadeOut(200);
    }
  });
}
$(window).resize(() => {
  if (isTouchDevice()) {
    $(".om-drop-link-monitorujeme").click(function () {
      $(".megamenu-monitorujeme").fadeIn(200);
    });
    $(window).click(function (e) {
      if (!e.target.className.includes("om-drop-link-sluzby")) {
        $(".megamenu-monitorujeme").fadeOut(200);
      }
    });
    $(".om-drop-link-sluzby").click(function () {
      $(".megamenu-sluzby").fadeIn(200);
    });
    $(window).click(function (e) {
      if (!e.target.className.includes("om-drop-link-sluzby")) {
        $(".megamenu-sluzby").fadeOut(200);
      }
    });
  } else {
    $(window).mouseover(function () {
      if ($(".om-drop-link-sluzby:hover").length != 0) {
        open = true;
        if (intervalset) {
          clearInterval(interval);
          intervalset = false;
        }
        setTimeout(() => {
          open = false;
          intervalset = false;
        }, 1000);
      }
      if (
        $(".megamenu-sluzby:hover").length != 0 ||
        open ||
        $(".om-drop-link-sluzby:hover").length != 0
      ) {
        $(".megamenu-sluzby").fadeIn(200);
      } else {
        $(".megamenu-sluzby").fadeOut(200);
      }
    });

    $(window).mouseover(function () {
      if ($(".om-drop-link-monitorujeme:hover").length != 0) {
        open2 = true;
        if (intervalset2) {
          clearInterval(interval2);
          intervalset2 = false;
        }
        setTimeout(() => {
          open2 = false;
          intervalset2 = false;
        }, 1000);
      }
      if (
        $(".megamenu-monitorujeme:hover").length != 0 ||
        open2 ||
        $(".om-drop-link-monitorujeme:hover").length != 0
      ) {
        $(".megamenu-monitorujeme").fadeIn(200);
      } else {
        $(".megamenu-monitorujeme").fadeOut(200);
      }
    });
  }
});
$(".close-button").click(function (e) {
  $(".navbar-collapse").removeClass("show");
  $("body").removeClass("offcanvas-active");
});

$(window).scroll(function () {
  if ($(this).scrollTop() >= 500) {
    // If page is scrolled more than 50px
    $("#return-to-top").fadeIn(200); // Fade in the arrow
  } else {
    $("#return-to-top").fadeOut(200); // Else fade out the arrow
  }
});
$("#return-to-top").click(function () {
  // When arrow is clicked
  $("body,html").animate(
    {
      scrollTop: 0, // Scroll to top of body
    },
    500
  );
});
